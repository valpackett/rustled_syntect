defmodule RustledSyntect.MixProject do
  use Mix.Project

  def project do
    [
      app: :rustled_syntect,
      description:
        "Rustler binding for the Syntect syntax highlighter, with streaming and iolists",
      version: "0.2.1",
      elixir: "~> 1.10",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package()
    ]
  end

  def application do
    []
  end

  defp deps do
    [
      {:rustler, "~> 0.25.0"},
      {:ex_doc, ">= 0.0.0", only: :dev}
    ]
  end

  defp package do
    [
      files: ["native", "lib", "mix.exs", "README.md", "LICENSE.txt"],
      maintainers: ["Val Packett"],
      licenses: ["MIT"],
      links: %{"Codeberg" => "https://codeberg.org/valpackett/rustled_syntect"}
    ]
  end
end
